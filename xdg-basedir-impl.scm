;;; Implementation of the XDG Base Directory Specification 0.8
;;; http://standards.freedesktop.org/basedir-spec/basedir-spec-0.8.html

(import chicken scheme)
(use files data-structures srfi-1 posix)

(define current-application
  (make-parameter #f))


;;; Internal helper functions

(define (application)
  (or (current-application)
      (error "Parameter current-application is not set")))

(define ((home path))
  (make-pathname (get-environment-variable "HOME") path))

(define (blank? x)
  (or (not x) (zero? (string-length x))))

(define ((dir-from-env/default env-var get-default))
  (let ((dir (get-environment-variable env-var)))
    (cond ((blank? dir)
           (get-default))
          ((not (absolute-pathname? dir))
           (error (string-append env-var " must not be a relative path") dir))
          (else dir))))

(define ((dirs-from-env/default env-var default))
  (let ((dirs (get-environment-variable env-var)))
    (if (blank? dirs)
        default
        (filter absolute-pathname? (string-split dirs ":")))))

(define ((find-file get-home-base-dir get-base-dirs) path)
  (let ((app-path (make-pathname (application) path)))
    (let find ((base-dirs (cons (get-home-base-dir) (get-base-dirs))))
      (and (not (null? base-dirs))
           (or (file-exists? (make-pathname (car base-dirs) app-path))
               (find (cdr base-dirs)))))))

(define parent-directory-creation-mode
  (string->number "077" 8))

(define ((home-dir get-home-base-dir) path)
  (let* ((app-home  (make-pathname (get-home-base-dir) (application)))
         (full-path (make-pathname app-home path)))
    (parameterize ((file-creation-mode parent-directory-creation-mode))
      (create-directory full-path #t))))

(define ((home-file make-home-dir) path)
  (make-pathname (make-home-dir (pathname-directory path))
                 (pathname-strip-directory path)))



;;; XDG_DATA_HOME, XDG_DATA_DIRS

(define data-home
  (dir-from-env/default "XDG_DATA_HOME" (home ".local/share")))

(define data-directories
  (dirs-from-env/default "XDG_DATA_DIRS" '("/usr/local/share/" "/usr/share/")))

(define find-data-file
  (find-file data-home data-directories))

(define data-directory
  (home-dir data-home))

(define data-file
  (home-file data-directory))


;;; XDG_CONFIG_HOME, XDG_CONFIG_DIRS

(define config-home
  (dir-from-env/default "XDG_CONFIG_HOME" (home ".config")))

(define config-directories
  (dirs-from-env/default "XDG_CONFIG_DIRS" '("/etc/xdg")))

(define find-config-file
  (find-file config-home config-directories))

(define config-directory
  (home-dir config-home))

(define config-file
  (home-file config-directory))


;;; XDG_CACHE_HOME

(define cache-home
  (dir-from-env/default "XDG_CACHE_HOME" (home ".cache")))

(define cache-directory
  (home-dir config-home))

(define cache-file
  (home-file cache-directory))


;;; XDG_RUNTIME_DIR
;;
;; TODO: Implement fall back to a replacement directory if not set

(define (runtime-home)
  (get-environment-variable "XDG_RUNTIME_DIR"))

(define runtime-directory
  (home-dir runtime-home))

(define runtime-file
  (home-file runtime-directory))
