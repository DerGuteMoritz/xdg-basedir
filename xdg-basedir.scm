(module xdg-basedir

(current-application

 data-home
 data-directories
 find-data-file
 data-directory
 data-file

 config-home
 config-directories
 find-config-file
 config-directory
 config-file

 cache-home
 cache-directory
 cache-file

 runtime-home
 runtime-directory
 runtime-file)

"xdg-basedir-impl.scm"

)
